<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class CategoryIndexTest extends TestCase
{
	public function test_it_return_a_collecttion_of_categories()
	{
		$categories = factory(\App\Models\Category::class)->create();

		$response = $this->json('GET', 'api/categories');

		$categories->each(function ($category) use ($response) {
			$response->assertJsonFragment([
				'slug' => $category->slug
			]);
		});
	}

	public function test_it_returns_only_parent_categories()
	{
		$category = factory(\App\Models\Category::class)->create();

		$category->children()->save(
			factory(\App\Models\Category::class)->create()
		);

		$this->json('GET', 'api/categories')
			->assertJsonCount(1, 'data');
	}

	public function test_it_returns_categories_ordered_by_their_given_order()
	{
		$category = factory(\App\Models\Category::class)->create([
			'order' => 2
		]);

		$anothercategory = factory(\App\Models\Category::class)->create([
			'order' => 1
		]);

		$this->json('GET', 'api/categories')
			->assertSeeInOrder([
				$anothercategory->slug, $category->slug
			]);
	}
}
