<?php

namespace Tests\Unit\Models\Categories;

use Tests\TestCase;

class CategoryTest extends TestCase
{
    public function test_it_many_children()
    {
        $category = factory(\App\Models\Category::class)->create();

        $category->children()->save(
            factory(\App\Models\Category::class)->create()
        );

        $this->assertInstanceOf(\App\Models\Category::class, $category->children->first());
    }

    public function test_it_can_fetch_only_parents()
    {
        $category = factory(\App\Models\Category::class)->create();

        $category->children()->save(
            factory(\App\Models\Category::class)->create()
        );

        $this->assertEquals(1,\App\Models\Category::parents()->count());
    }

    public function test_it_is_orderable_by_numbered_order()
    {
        $category = factory(\App\Models\Category::class)->create([
            'order' => 2
        ]);

        $anotherCategory = factory(\App\Models\Category::class)->create([
            'order' => 1
        ]);

        $this->assertEquals($anotherCategory->name,\App\Models\Category::ordered()->first()->name);
    }
}
